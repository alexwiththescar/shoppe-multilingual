class AddPaypalToShoppeOrders < ActiveRecord::Migration
  def change
    add_column :shoppe_orders, :express_token, :string
    add_column :shoppe_orders, :express_payer_id, :string
    add_column :shoppe_orders, :transaction_id, :string
  end
end
