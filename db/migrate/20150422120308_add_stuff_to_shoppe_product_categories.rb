class AddStuffToShoppeProductCategories < ActiveRecord::Migration
  def change
    add_column :shoppe_product_categories, :name_es, :string
    add_column :shoppe_product_categories, :description_es, :text

    rename_column :shoppe_product_categories, :name, :name_en
    rename_column :shoppe_product_categories, :description, :description_en

  end
end
