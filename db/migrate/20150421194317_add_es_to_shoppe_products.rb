class AddEsToShoppeProducts < ActiveRecord::Migration
  def change
    add_column :shoppe_products, :short_description_es, :text
    add_column :shoppe_products, :description_es, :text
    add_column :shoppe_products, :name_es, :text
    add_column :shoppe_products, :in_the_box_es, :text

    rename_column :shoppe_products, :short_description, :short_description_en
    rename_column :shoppe_products, :description, :description_en
    rename_column :shoppe_products, :name, :name_en
    rename_column :shoppe_products, :in_the_box, :in_the_box_en
  end
end
