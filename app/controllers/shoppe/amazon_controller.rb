module Shoppe
  class AmazonController < Shoppe::ApplicationController

    def request_ec
      request_ec = Vacuum.new('GB')
      request_ec.version = '2011-08-01'
      request_ec.associate_tag = ENV["EC2_ASSOSIATE_TAG"] ||= 'tag'
      return request_ec
    end

    def amazon_product(item_id)
     
      item = {}
      item_info = request_ec.item_lookup( query: { 'ResponseGroup' => 'Large', 'ItemId' => item_id  } ).to_h
      item_info = item_info["ItemLookupResponse"]["Items"]["Item"]
      item = {
        :item_id => item_info["ASIN"],
        :link => item_info["DetailPageURL"],
        :img_small => item_info["SmallImage"],
        :img_med =>item_info["MediumImage"],
        :img_large => item_info["LargeImage"],
        :brand => item_info["ItemAttributes"]["Brand"],
        :title => item_info["ItemAttributes"]["Title"],
        :price => item_info["Offers"]["Offer"]["OfferListing"]["Price"] 
              }
      return item
    end

    def new
      @item = amazon_product(params[:item_id])
      @product = Shoppe::Product.new(:reference => "amazon", :ref_url => @item[:link])
      # code to add amazon product into DB goes here
      # Will need to add AMAZON COLOUMN to defirentiate products origin.


    end

    def find

    end
  end
end
